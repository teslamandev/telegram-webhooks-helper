#! env python
# -*- coding: utf-8 -*-
import argparse
import json
import os
import urllib
from collections import abc
from urllib.request import urlopen
from urllib.error import HTTPError


def main():
    args = parse_args()
    cleaned_args = validate(args)
    args.func(cleaned_args)


def parse_args():
    parser = argparse.ArgumentParser(description="Simplify work with telegram webhooks")
    subparsers = parser.add_subparsers(dest="command", required=True)

    check = subparsers.add_parser("check", help="Check webhook info")
    set_ = subparsers.add_parser("set", help="Set webhook url")
    delete = subparsers.add_parser("delete", help="Delete webhook url")

    check.add_argument("-t", "--token", help="Telegram bot token")
    check.set_defaults(func=check_webhook)

    set_.add_argument("-t", "--token", help="Telegram bot token")
    set_.add_argument("-w", "--webhook", help="Webhook url to set")
    set_.set_defaults(func=set_webhook)

    delete.add_argument("-t", "--token", help="Telegram bot token")
    delete.set_defaults(func=delete_webhook)

    return parser.parse_args()


def validate(args):
    """If not enough information is provided - find it in environment variables and validate."""
    token, webhook = check_for_arguments(args)
    validate_token_format(token)
    return dict(token=token, webhook=webhook)


def check_for_arguments(args):
    exit_message = "Token is not provided (set it with '-t' parameter or env var 'TOKEN')"
    token = args.token or os.environ.get("TOKEN") or exit(exit_message)

    if "webhook" in args:
        exit_message = "Webhook url is not provided (set it with '-w' parameter or env var 'WEBHOOK')"
        webhook = args.webhook or os.environ.get("WEBHOOK") or exit(exit_message)
    else:
        webhook = ""

    token = token.strip()
    webhook = webhook.strip()

    return token, webhook


def validate_token_format(token) -> None:
    if any(x.isspace() for x in token):
        exit_message = "Token is invalid: '{}'".format(token)
        exit(exit_message)

    left, sep, right = token.partition(':')

    if (not sep) or (not left.isdigit()) or (len(left) < 3) or (len(right) < 3):
        exit_message = "Token is invalid: '{}'".format(token)
        exit(exit_message)


def check_webhook(args):
    url = "https://api.telegram.org/bot{token}/getWebhookinfo".format(token=args["token"])
    response = request_telegram_api(url)
    print_api_response(args["token"], response)


def set_webhook(args):
    url = "https://api.telegram.org/bot{token}/setWebhook".format(token=args["token"])
    webhook_data = {"url": args["webhook"]}
    response = request_telegram_api(url, webhook_data)
    print_api_response(args["token"], response)


def delete_webhook(args):
    url = "https://api.telegram.org/bot{token}/deleteWebhook".format(token=args["token"])
    response = request_telegram_api(url)
    print_api_response(args["token"], response)


def request_telegram_api(url, webhook_data=None):
    data = encode(webhook_data)
    return request(url, data)


def encode(webhook_data):
    try:
        encoded = urllib.parse.urlencode(webhook_data).encode()
    except TypeError:
        encoded = urllib.parse.urlencode(dict()).encode()
    return encoded


def request(url, data):
    try:
        response = urlopen(url, data=data).read()
    except HTTPError as e:
        exit("> HTTP Error: [{}] {}".format(e.code, e.reason))
    return json.loads(response, encoding="utf-8")


def print_api_response(token, response):
    print("Token: {}\n".format(token))
    for key, val in flatten(response):
        print("> {}: {}".format(key, val))


def flatten(obj: dict):
    for key in obj:
        if isinstance(obj[key], abc.MutableMapping):
            yield from flatten(obj[key])
        else:
            yield key, obj[key]


if __name__ == '__main__':
    main()
