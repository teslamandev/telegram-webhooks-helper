# Telegram webhooks helper
This small script can simplify your work with telegram bots' webhooks.
It has three methods: set the webhook, check the webhook state and delete webhook.

## Requirements
- The script has no external requirements.

## Usage
- python wh.py check|set|delete [-t <TOKEN>] [-w <WEBHOOK URL>]

#### Example

- python wh.py check -t 613647140:AAKXXrcLbKuf0J3gZDGbJlvK089b3M1q2R4
- python wh.py set -t 613647140:AAKXXrcLbKuf0J3gZDGbJlvK089b3M1q2R4 -w https://example.com
- python wh.py delete -t 613647140:AAKXXrcLbKuf0J3gZDGbJlvK089b3M1q2R4


## Setting environment variables
You can set args via environment variables 'TOKEN' and 'WEBHOOK' and launch script without these args.

__Linux and Mac:__

- export TOKEN=11613647140:AAKXXrcLbKuf0J3gZDGbJlvK089b3M1q2R4
- export WEBHOOK=https://example.com

__Windows:__

- set TOKEN "1613647140:AAKXXrcLbKuf0J3gZDGbJlvK089b3M1q2R4"
- set WEBHOOK "https://example.com"

__Now you can start the script without arguments. They'll be taken from env vars.__

#### Example

- python wh.py set
- python wh.py check
- python wh.py delete

## More info
See **python wh.py --help** for more information.
